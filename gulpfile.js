'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sassGlob = require('gulp-sass-glob'),
    concat = require('gulp-concat'),
    concatCss = require('gulp-concat-css'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch'),
    jshint = require('gulp-jshint'),
    notify = require("gulp-notify"),
    browserSync = require('browser-sync').create(),
    util = require('gulp-util');

/*
 Edit project name and feel free to edit input outputs
 */
var config = {
    projectName: 'lab',
    sassDir: 'src/sass/**/*.scss',
    cssOutput: 'dist/css',
    jsInput: ['dist/js/vendor.js', 'dist/js/scripts.js'],
    jsDir: 'src/js/scripts/*.js',
    jsVendorDir: ['src/js/vendor/lib/*.js', 'src/js/vendor/plugins/*.js'],
    jsOutput: 'dist/js/',
    production: !!util.env.production
};

gulp.task('compileCss', function () {
    return gulp.src(config.sassDir)
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        //dont compress in production
        //.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest(config.cssOutput))
        .pipe(browserSync.stream())
        .pipe(notify({message: 'SASS compilation complete', onLast: true}));
});

gulp.task('compileVendorJs', function () {
    return gulp.src(config.jsVendorDir)
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.jsOutput))
        .pipe(browserSync.stream())
        .pipe(notify({message: 'JS compilation complete', onLast: true}));
});

gulp.task('compileJs', function (done) {
    return gulp.src(config.jsDir)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'))
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest(config.jsOutput))
        .pipe(browserSync.stream())
        .pipe(notify("JS compilation complete"), done);
});

// Static Server + watching scss/html files
gulp.task('browser-sync', function () {
    browserSync.init({
        server: "./"
    });
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('watch', function () {
    gulp.watch('src/sass/**/*.scss', ['compileCss']);
    gulp.watch('src/js/**/*.js', ['compileJs'])
    gulp.watch('src/vendor/js/*.js', ['compileVendorJs'])
});

gulp.task('watch-bsync', function () {
    gulp.start('browser-sync');
    gulp.watch('src/sass/**/*.scss', ['compileCss']);
    gulp.watch('src/js/**/*.js', ['compileJs'])
    gulp.watch('src/vendor/js/*.js', ['compileVendorJs'])
});
