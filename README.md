# Boilerplate for project using GULP SASS and Susy grid system

### Gulp Tasks
* Gulp compileCss
* Gulp compileJs
* Gulp compileVendorJS
* Gulp compileVendorCss

### Gulp watch
Watch all file and run approiate task from above

### Whats included
* Gulp
* Gulp Watch
* Gulp Sass
* Gulp ConcatCSS
* Gulp JShint
* Gulp Notify
* Susy
* Breakpoint sass
* jQuery

### Whats this for
Gulp set up to start projects.
Drop into Wordpress, Magento or even flat html project

### Version
1

### To do
* Implement bower concat
* Impelment import globbing
* Improve JS hint 